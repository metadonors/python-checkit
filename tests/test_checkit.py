import os 
from pycheckit import Checkit
import logging

logging.basicConfig(level=logging.DEBUG)
CHECKIT_API_KEY = os.getenv('CHECKIT_API_KEY', False)


def test_validate_creditcard():
    number = "424242424242"

    checkit = Checkit(api_key=CHECKIT_API_KEY)

    result = checkit.creditcards.check(number)

    assert isinstance(result, dict)
    assert isinstance(result['result'], bool)
    assert result['number'] == number

def test_validate_iban():
    iban = "IT60X0542811101000000123456"

    checkit = Checkit(api_key=CHECKIT_API_KEY)

    result = checkit.iban.check(iban)

    assert isinstance(result, dict)
    assert isinstance(result['result'], bool)
    assert result['iban'] == iban
